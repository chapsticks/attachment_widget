```
#控件使用说明
--model字段设置
--8.0版本老的api
class demo(osv.osv):

    def _data_get(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        datas = []
        att = self.pool.get('ir.attachment')
        attIds = att.search(cr, uid, [('res_model','=',self._name),('res_id','=',ids[0])])
        attObjs = att.browse(cr, uid, attIds)
        for i in attObjs:
            if i.file_type_icon!='webimage':
                html = """<div class='oe_attachment oe_ff' data-id="%(attId)s"><a href='/mail/download_attachment?model=%(parentModel)s&id=%(parendId)s&method=download_attachment&attachment_id=%(attId)s' target='_blank'>
                        <img src='/mail/static/src/img/mimetypes/%(attIcon)s.png' />
                        <div class='oe_name' style='word-break: break-all;'>%(filename)s</div>
                    </a></div>""" % {
                                'parentModel':self._name,
                                'parendId':ids[0],
                                'attId':i.id,
                                'attIcon':(i.file_type_icon or 'unknown'),
                                'filename':i.name
                            }
            else:
                html = """<div class='oe_attachment oe_ff oe_preview' data-id="%(attId)s"><a href='/mail/download_attachment?model=%(parentModel)s&id=%(parendId)s&method=download_attachment&attachment_id=%(attId)s' target='_blank'>
                        <img src='/web/binary/image?model=ir.attachment&field=datas&id=%(attId)s&resize=100%%252C80' />
                        <div class='oe_name' style='word-break: break-all;'>%(filename)s</div>
                        </a></div>""" % {
                                'parentModel':self._name,
                                'parendId':ids[0],
                                'attId':i.id,
                                'filename':i.name
                            }
            datas.append({
                'id':i.id,
                'file_type_icon':i.file_type_icon,
                'datas_fname':i.datas_fname,
                'data':att._file_read(cr, uid, i.store_fname),
                'html':html
            })
        return {ids[0]:datas}

    def _data_set(self, cr, uid, id, name, value, arg, context=None):
        return True

    _columns = {
        'datas': fields.function(_data_get, fnct_inv=_data_set, string='测试附件', type="binary", nodrop=True),
    }

8.0版本新的api
from openerp import models, fields

class demo(models.Model):

    _inherit = 'demo'


    datas = fields.Binary(compute='_data_get', inverse='_inverse_display_name',  string='测试附件')

    @api.one
    def _data_get(self):
        context = self.env.context
        if context is None:
            context = {}
        datas = []
        att = self.env['ir.attachment']
        attObjs = att.search([('res_model','=',self._name),('res_id','=',self.id)])
        for i in attObjs:
            if i.file_type_icon!='webimage':
                html = """<div class='oe_attachment oe_ff' data-id="%(attId)s"><a href='/mail/download_attachment?model=%(parentModel)s&id=%(parendId)s&method=download_attachment&attachment_id=%(attId)s' target='_blank'>
                        <img src='/mail/static/src/img/mimetypes/%(attIcon)s.png' />
                        <div class='oe_name' style='word-break: break-all;'>%(filename)s</div>
                    </a></div>""" % {
                                'parentModel':self._name,
                                'parendId':self.id,
                                'attId':i.id,
                                'attIcon':(i.file_type_icon or 'unknown'),
                                'filename':i.name
                            }
            else:
                html = """<div class='oe_attachment oe_ff oe_preview' data-id="%(attId)s"><a href='/mail/download_attachment?model=%(parentModel)s&id=%(parendId)s&method=download_attachment&attachment_id=%(attId)s' target='_blank'>
                        <img src='/web/binary/image?model=ir.attachment&field=datas&id=%(attId)s&resize=100%%252C80' />
                        <div class='oe_name' style='word-break: break-all;'>%(filename)s</div>
                        </a></div>""" % {
                                'parentModel':self._name,
                                'parendId':self.id,
                                'attId':i.id,
                                'filename':i.name
                            }
            datas.append({
                'id':i.id,
                'file_type_icon':i.file_type_icon,
                'datas_fname':i.datas_fname,
                'data':att._file_read(i.store_fname),
                'html':html
            })
        self.datas = datas

    @api.one
    def _inverse_display_name(self):
        pass




xml控件使用说明
<field name="datas" widget="ff_file_widget"/>
```