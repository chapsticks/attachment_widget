# -*- coding: utf-8 -*-
{
    'name': "Attachment Widget",
    'summary': '字段附件控件',
    'version': '1.0',
    'category': 'Tools',
    'author': '上海-Allen',
    'description': """
    """,
    'depends': ['base','web'],
    'data': [
        'view/attachment_widget.xml',
    ],
    'qweb': ['static/src/xml/*.xml'],
    'website': 'http://www.freshfresh.com',
    'installable': True,
}
